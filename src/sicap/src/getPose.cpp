#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovariance.h"

void receiveAmcl(const boost::shared_ptr<const geometry_msgs::PoseWithCovarianceStamped> amcl_pose)
{
  ROS_INFO("I heard something"); // This is never printed
  //Some more code goes here, haven't included it yet
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "subscriber");
  ros::NodeHandle n;
  ros::Subscriber amcl_sub = n.subscribe<geometry_msgs::PoseWithCovarianceStamped>("amcl_pose", 100,  receiveAmcl);
  ros::spin();
  return 0;
}