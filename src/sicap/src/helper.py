#!/usr/bin/env python
import roslib; roslib.load_manifest('sicap')
import rospy
import tf.transformations
from geometry_msgs.msg import Twist
import os
from subprocess import call
from actionlib_msgs.msg import GoalStatusArray

def callback(msg):
    rospy.loginfo("callback")
    rospy.loginfo(msg['status'])
    rospy.sleep(10)

def listener():
    rospy.init_node('sicap_helper')
    #navigation_velocity_smoother/raw_cmd_vel
    rospy.Subscriber("/move_base/status", GoalStatusArray, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
