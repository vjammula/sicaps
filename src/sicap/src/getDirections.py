#!/usr/bin/env python
import roslib; roslib.load_manifest('sicap')
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import PoseWithCovarianceStamped

imagePath = ""
prevZ = 0

def setImagePath(image_path):
    #rospy.loginfo(image_path)
    global imagePath
    imagePath = image_path
    rospy.loginfo(imagePath)



def getImagePath():
    return imagePath

def callback(msg):
    global prevZ
    prevZ = 0
    pose = msg.pose
    rospy.loginfo("Received a /mobile_base/commands/velocity message!")
    rospy.loginfo("Linear Components: [%f, %f, %f]" % (pose.pose.position.x, pose.pose.position.y, pose.pose.position.z))
    rospy.loginfo("Angular Components: [%f, %f, %f]" % (pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z))
    if prevZ != 0 and prevZ != msg.angular.z:
        rospy.loginfo("Changed direction....")
    prevZ = pose.pose.orientation.z
    rospy.sleep(5)

def listener():
    rospy.init_node('sicap_vel_listener')

    rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
