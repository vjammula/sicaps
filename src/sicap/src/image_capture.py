#!/usr/bin/env python

from __future__ import print_function
import sys
import time
import rospy
import cv2
import rospkg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from subprocess import call
import os

class TakePhoto:
    def __init__(self):

        self.bridge = CvBridge()
        self.image_received = False

        # Connect image topic
        img_topic = "/camera/rgb/image_raw"
        self.image_sub = rospy.Subscriber(img_topic, Image, self.callback)

        # Allow up to one second to connection
        rospy.sleep(1)

    def callback(self, data):

        # Convert image to OpenCV format
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        self.image_received = True
        self.image = cv_image

    def take_picture(self, img_title):
        if self.image_received:
            # Save an image
            imagePath = rospkg.RosPack().get_path('sicap') + '/imgs/'
            imagePath = imagePath + img_title
            cv2.imwrite(imagePath, self.image)
            # call the densecap
            self.getCaptions(imagePath)
            return True
        else:
            return False

    def getCaptions(self, imagePath):
        if len(imagePath) != 0:
            rospy.loginfo("getCaptions....")
            scriptPath = "/home/varun/Dropbox (ASU)/cse591/densecap/"
            os.chdir(scriptPath)
            call(["th", "run_model.lua", "-input_image", imagePath])
            rospy.sleep(10)

if __name__ == '__main__':

    # Initialize
    rospy.init_node('sicap_image_densecap')
    rospy.sleep(6)
    try:
        while 1:
            # Take a photo
            camera = TakePhoto()
            # Use '_image_title' parameter from command line

            title_tag = 'photo-{0}.jpg'.format(time.time())
            img_title = rospy.get_param('~image_title', title_tag)

            if camera.take_picture(img_title):
                rospy.loginfo("Saved image " + img_title)
            else:
                rospy.loginfo("No images received")

            # Sleep to give the last log messages time to be sent
            rospy.sleep(5)

    except:
        rospy.loginfo("Error occured in sicap_image_densecap")
        sys.exit()