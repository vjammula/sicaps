#!/usr/bin/env python


import rospy
import sys
# from std_msgs.msg import String
from geometry_msgs.msg import Twist


def talker():
    rospy.init_node('sicap_navigation_controller', anonymous=True)

    pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=10)

    rate = rospy.Rate(1)  # 10hz
    i = 0
    while not rospy.is_shutdown():
        rate.sleep()
        cmd_vel = Twist()

        cmd_vel.linear.x = 4.0
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = -2.0
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = 0.0
        cmd_vel.angular.z = -1.5
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = 3.0
        cmd_vel.angular.z = 0.0
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = 1.0
        cmd_vel.angular.z = -1.5
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = 0.6
        cmd_vel.linear.y = 0.6
        cmd_vel.angular.z = -1.5
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
        #
        cmd_vel.linear.x = 0.6
        cmd_vel.linear.y = 0.0
        cmd_vel.angular.z = 0.0
        rospy.loginfo(cmd_vel)
        pub.publish(cmd_vel)
        rate.sleep()
    sys.exit()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
